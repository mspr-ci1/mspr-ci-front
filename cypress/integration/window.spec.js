/// <reference types="cypress" />

// eslint-disable-next-line no-undef
context('Window', () => {
  // eslint-disable-next-line no-undef
  beforeEach(() => {
    // eslint-disable-next-line no-undef
    cy.visit("/")
  })

  // eslint-disable-next-line no-undef
  it('cy.window() - get the global window object', () => {
    // https://on.cypress.io/window
    // eslint-disable-next-line no-undef
    cy.window().should('have.property', 'top')
  })

  // eslint-disable-next-line no-undef
  it('cy.document() - get the document object', () => {
    // https://on.cypress.io/document
    // eslint-disable-next-line no-undef
    cy.document().should('have.property', 'charset').and('eq', 'UTF-8')
  })

  // eslint-disable-next-line no-undef
  it('cy.title() - get the title', () => {
    // https://on.cypress.io/title
    // eslint-disable-next-line no-undef
    cy.title().should('include', 'AYAMarket')
  })
})

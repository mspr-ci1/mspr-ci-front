/// <reference types="cypress" />
// eslint-disable-next-line no-undef
describe("The home page shows something", () => {
    // eslint-disable-next-line no-undef
    it("shows a title", () => {
      // eslint-disable-next-line no-undef
      cy.visit("/");
      // eslint-disable-next-line no-undef
      cy.contains("Clients");
    });
  });
  
  // eslint-disable-next-line no-undef
  describe("The home page shows an error when the year is invalid", () => {
      // eslint-disable-next-line no-undef
      it("shows an error", () => {
        // eslint-disable-next-line no-undef
        cy.visit("/");
        // eslint-disable-next-line no-undef
        cy.get("#submit-btn").click()
        // eslint-disable-next-line no-undef
        cy.contains("L'année n'est pas renseignée");
      });
    });
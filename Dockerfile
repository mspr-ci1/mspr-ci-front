FROM node:8-alpine

WORKDIR /

ADD package.json /package.json

RUN npm config set registry http://registry.npmjs.org

RUN npm install

ADD . /

RUN npm run build

EXPOSE 8080

CMD ["npm", "run", "serve"]
